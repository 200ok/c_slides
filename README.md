# c_slides

In this repository a web application is being created that will be
compatible with [https://github.com/munen/p_slides](p_slides), but:

- It will be programmed in CLJS
- Not only will it render Markdown to HTML, it will also have a web
  interface to create new slides

## Running

    lein run
    lein figwheel

## Deployment

This application has a demo deployment: https://c-slides.herokuapp.com/

If your app isn't configured for Heroku deployment, yet:

    git remote add heroku https://git.heroku.com/c-slides.git

New deployment is easy with:

    git push heroku master
