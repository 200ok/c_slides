(ns c-slides.core
    (:require [reagent.core :as reagent :refer [atom]]
              [secretary.core :as secretary :include-macros true]
              [accountant.core :as accountant]
              [cljsjs.highlight]
              [cljsjs.highlight.langs.javascript]
              [cljsjs.highlight.langs.clojure]
              [cljsjs.highlight.langs.css]
              [cljsjs.highlight.langs.xml]
              [cljsjs.showdown]))


;; --------------------------------------
;; State
(defonce slides (reagent/atom {:markdown ""
                               :slides [""]
                               :page 0}))

(defonce converter (new js/showdown.Converter))

;; --------------------------------------
;; Helpers

(defn- md2html [md]
  (.makeHtml converter md))

(defn- split-slides [md]
  (map #(str "<div class='slide'>" % "</div>")
       (clojure.string/split md #"<hr />")))

;; -------------------------
;; Views

(defn- input []
  [:div {:id "markdown-input"}
   [:h2 "Markdown"]
   [:textarea {:value (:markdown @slides)
               :on-change (fn [e]
                            (swap! slides assoc :markdown
                                   (-> e .-target .-value))
                            (swap! slides assoc :slides
                                   (split-slides (md2html (:markdown @slides)))))}]])

(defn- highlight-active-slide []
  (goog.object/forEach
   ;; TODO: Write selector that only fetches the current slide
   ;; XXX: Alternatively use RemarkableJS which has proper HighlightJS
   ;;      integration (but is not available on cljsjs.
   (.querySelectorAll js/document "pre code")
   (fn [e] (try (.highlightBlock js/hljs e)
                ;; HighlightJS will throw an error every time. This
                ;; might just be a bug in the current version. Since
                ;; it works, I'm ignoring all exceptions thrown by it.
                (catch js/Error e)))))

(defn inc-page []
  (if (> (dec (count (:slides @slides)))
         (:page @slides) )
    (swap! slides update-in [:page] inc)))


(defn dec-page []
  (if (> (:page @slides) 0)
    (swap! slides update-in [:page] dec)))

(defn- output []
  (reagent/create-class
   {
    :component-did-update #(highlight-active-slide)
    :component-did-mount #(highlight-active-slide)
    :reagent-render
    (fn []
      [:div {:id "html-output"}
       [:h2 "Generated"]
       [:div {:dangerouslySetInnerHTML {:__html (nth (:slides @slides)
                                                     (:page @slides))}}]
       [:div.current-page
        [:span {:on-click #(dec-page)} "<" ]
        [:span " " (inc (:page @slides)) " of " (count (:slides @slides)) " "]
        [:span {:on-click #(inc-page)} ">" ]]])}))

(defn home-page []
  [:div [:h2 "Welcome to c_slides"]
   [:p "Enter some markdown on the left and see your slides generated
   on the right. Separate slides by with ---."]
   [input]
   [output]])

;; -------------------------
;; Routes

(def page (atom #'home-page))

(defn current-page []
  [:div [@page]])

(secretary/defroute "/" []
  (reset! page #'home-page))

;; -------------------------
;; Initialize app

(defn mount-root []
  (reagent/render [current-page] (.getElementById js/document "app")))

(defn init! []
  (accountant/configure-navigation!
    {:nav-handler
     (fn [path]
       (secretary/dispatch! path))
     :path-exists?
     (fn [path]
       (secretary/locate-route path))})
  (accountant/dispatch-current!)
  (mount-root))


;; -------------------------
;; Shortcuts

(def ^:private key-events
  {
   37 dec-page   ; Left
   39 inc-page   ; Right
   })

(defn- key-handler-fallback [event]
  (.log js/console (str "keyCode: " (.-keyCode event))))

(defn- key-handler [event]
  ;;(.preventDefault event)
  (if-let [fun (key-events (.-keyCode event))]
    (fun)
    (key-handler-fallback event)))

(defn register-global-key-handler []
  (aset js/document "onkeydown" key-handler))

(register-global-key-handler)
