(ns c-slides.prod
  (:require [c-slides.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
